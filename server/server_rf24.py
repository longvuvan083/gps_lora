from http import client
import RPi.GPIO as GPIO
from lib_nrf24 import NRF24
import csv 
import time
from datetime import datetime
GPIO.setmode(GPIO.BCM) 
import time 
import spidev 
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-t", "--LogTime", default= 300 ,help= "the duration time in second for log the data into csv file",
                    type=int)
args = parser.parse_args()
          
client_address = [0x01, 0x02, 0x03, 0x04, 0x05]
radio = NRF24(GPIO, spidev.SpiDev()) 
radio.begin(0, 17)

def init_reading_pipe() :
    idx = 1
    for client_addrs in client_address :
        radio.openReadingPipe(idx, client_addrs)
        idx +=1 

def main():

    radio.setPayloadSize(32) 
    radio.setChannel(80) 
    radio.setPALevel(NRF24.PA_LOW)
    radio.setDataRate(NRF24.BR_250KBPS)

    radio.setAutoAck(True) 
    radio.enableDynamicPayloads() 
    radio.enableAckPayload()

    init_reading_pipe()

    radio.startListening() # start listening data from client
    receive_gps_data = []
    now = datetime.now().strftime("%d:%m:%-%H:%M")
    start_time = time.time()
    end_time = time.time()
    with open ("{}_target1.csv".format(now), 'w' ) as fclient:
        fclient.writelines("Client Number,GPS Raw Data")
       
        while True:
            while not radio.available(0):
                time.sllep(1/100)
            radio.read(receive_gps_data, radio.getDynamicPayloadSize())
            temp_str = ""
            for idx in receive_gps_data:
                if (idx >=32 and idx <= 126) :
                    temp_str += chr(idx) 
            fclient.writelines(temp_str)
         
            time.sleep(500)

            if end_time - start_time >= args.LogTime :
                fclient.close()
                break


if __name__ == "__main__":
    main()