#include <SparkFun_Ublox_Arduino_Library.h>
#include <RF24.h>
#include <nRF24L01.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <Wire.h> //Needed for I2C to GNSS

SFE_UBLOX_GPS  myGPS;
size_t numBytes = 0; // Record the number os bytes received from Serial1

RF24 radio(7,8); // nRF24L01 (CE,CSN)

const uint16_t client_address = 0x01;  // For other client, just replace by 0x02, 0x03, 0x04, 0x05
static const int gps_rx_Pin = 4;
static const int gps_tx_Pin = 3;
static const uint32_t GPSBaud = 9600;

void setup() {
  Serial.begin(115200);
  while (!Serial); //Wait for user to open terminal
    Serial.println("Serial opening...");
  Wire.begin();
  //Wire.setClock(400000); //Increase I2C clock speed to 400kHz
  if (myGPS.begin() == false) //Connect to the u-blox module using I2C port
  {
    Serial.println(F("u-blox GNSS not detected at default I2C address. Please check wiring. Freezing."));
    while (1);
  }
 //This will pipe all NMEA sentences to the serial port so we can see them
  myGPS.setNMEAOutputPort(Serial);
  
  //setting the RADIO
  SPI.begin();
  radio.begin();
  radio.openWritingPipe(client_address);
  radio.setPALevel(RF24_PA_LOW);
  radio.setChannel(80);
  radio.setDataRate(RF24_250KBPS);
  radio.stopListening();
  if (!radio.available()) {
    Serial.println("cannot connect to Receive radio");
    Serial.println("Retrying ...");
  }
}

void loop() {
  myGPS.checkUblox(); //See if new data is available. Process bytes as they come in.
  
  char  raw_gps[256];
  while ((Serial.available())) {
    char ch = Serial.read();
    if (ch == '\r' || ch == '\n') { //  check CR LF at the end of each nmea line
      numBytes = 0;
      break;  
    }
   raw_gps[numBytes] = ch; // the maximum length of nmea data is 128 byte
   numBytes +=1;
   delay(10);
  }

  String output_gps_data;
  if (radio.available()) {
      output_gps_data = String(client_address )+ "," + String(raw_gps);
  // write data
  radio.write(&output_gps_data, sizeof(output_gps_data));
  delay(200);
  }
}
